import domReady from '@wordpress/dom-ready';
import $ from 'jquery';
import 'slick-carousel';

domReady( function () {
	const sliders = document.querySelectorAll( '.beb-slider-gallery' );

	sliders.forEach( ( slider ) => {
		$( slider ).slick();
	} );
} );
