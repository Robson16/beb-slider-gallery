import { useBlockProps } from '@wordpress/block-editor';
import { __ } from '@wordpress/i18n';
import classnames from 'classnames';

export default function save( { attributes, style } ) {
	const {
		images,
		size,
		dots,
		infinite,
		speed,
		vertical,
		swipe,
		slidesToShow,
		slidesToScroll,
	} = attributes;

	const settings = JSON.stringify( {
		dots,
		infinite,
		speed,
		slidesToShow,
		slidesToScroll,
		vertical,
		verticalSwiping: swipe,
	} );

	const blockProps = useBlockProps.save( {
		className: classnames( {
			'beb-slider-gallery': true,
		} ),
		'data-slick': settings,
		style,
	} );

	return (
		<div { ...blockProps }>
			{ images.length > 0 ? (
				images.map( ( image ) => (
					<img
						key={ image.id }
						src={ image.sizes[ 'full' ].url }
						alt={ image.alt }
					/>
				) )
			) : (
				<p>{ __( 'Add some images!', 'beb-slider-gallery' ) }</p>
			) }
		</div>
	);
}
