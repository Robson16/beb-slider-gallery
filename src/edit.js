import { useBlockProps } from '@wordpress/block-editor';
import {
	InspectorControls,
	MediaUpload,
	MediaUploadCheck,
} from '@wordpress/blockEditor';
import {
	Button,
	PanelBody,
	RangeControl,
	SelectControl,
	ToggleControl,
} from '@wordpress/components';
import { Fragment, useState } from '@wordpress/element';
import { __ } from '@wordpress/i18n';
import classnames from 'classnames';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';
import './editor.scss';

export default function Edit( { attributes, setAttributes, style } ) {
	const {
		images,
		size,
		dots,
		infinite,
		speed,
		vertical,
		swipe,
		slidesToShow,
		slidesToScroll,
	} = attributes;

	const blockProps = useBlockProps( {
		className: classnames( {
			'beb-slider-gallery': true,
		} ),
		style,
	} );

	const onSelectMedia = ( media ) => {
		setAttributes( {
			images: media,
		} );
	};

	const settings = {
		dots,
		infinite,
		speed,
		slidesToShow,
		slidesToScroll,
		vertical,
		verticalSwiping: swipe,
	};

	return (
		<Fragment>
			<InspectorControls>
				<Fragment>
					<PanelBody
						title={ __(
							'Select the images',
							'beb-slider-gallery'
						) }
						initialOpen={ true }
					>
						<div className="editor-select-images">
							<MediaUploadCheck>
								<MediaUpload
									multiple={ true }
									gallery={ true }
									addToGallery={ true }
									onSelect={ ( media ) => {
										onSelectMedia( media );
									} }
									allowedTypes={ [ 'image' ] }
									value={ images.map( ( item ) => item.id ) }
									render={ ( { open } ) => {
										return (
											<Fragment>
												<Button
													variant="primary"
													onClick={ ( event ) => {
														event.stopPropagation();
														open();
													} }
												>
													{ images.length > 0
														? __(
																'Edit Images',
																'beb-slider-gallery'
														  )
														: __(
																'Select Images',
																'beb-slider-gallery'
														  ) }
												</Button>
											</Fragment>
										);
									} }
								/>
							</MediaUploadCheck>
						</div>
					</PanelBody>
					<PanelBody
						title={ __( 'Slider Option', 'beb-slider-gallery' ) }
						initialOpen={ true }
					>
						<ToggleControl
							label={ __( 'Dots', 'beb-slider-gallery' ) }
							checked={ dots }
							onChange={ () => {
								setAttributes( {
									dots: ! dots,
								} );
							} }
						/>
						<ToggleControl
							label={ __( 'Infinite', 'beb-slider-gallery' ) }
							checked={ infinite }
							onChange={ () => {
								setAttributes( {
									infinite: ! infinite,
								} );
							} }
						/>
						<ToggleControl
							label={ __( 'Swipeable', 'beb-slider-gallery' ) }
							checked={ swipe }
							onChange={ () => {
								setAttributes( {
									swipe: ! swipe,
								} );
							} }
						/>
						<ToggleControl
							label={
								vertical
									? `${ __(
											'Direction',
											'beb-slider-gallery'
									  ) } - ${ __(
											'Vertical',
											'beb-slider-gallery'
									  ) }`
									: `${ __(
											'Direction',
											'beb-slider-gallery'
									  ) } - ${ __(
											'Horizontal',
											'beb-slider-gallery'
									  ) }`
							}
							checked={ vertical }
							onChange={ () => {
								setAttributes( {
									vertical: ! vertical,
								} );
							} }
						/>
						<RangeControl
							label={ __( 'Speed', 'beb-slider-gallery' ) }
							help={ __(
								'Transition speed between slides in milliseconds',
								'beb-slider-gallery'
							) }
							value={ speed }
							onChange={ ( value ) => {
								setAttributes( {
									speed: value,
								} );
							} }
							min={ 300 }
							max={ 1000 }
						/>
						<RangeControl
							label={ __(
								'Slides to Show ',
								'beb-slider-gallery'
							) }
							value={ slidesToShow }
							onChange={ ( value ) => {
								setAttributes( {
									slidesToShow: value,
								} );
							} }
							min={ 1 }
							max={ images.length }
						/>
						<RangeControl
							label={ __(
								'Slides to Scroll ',
								'beb-slider-gallery'
							) }
							value={ slidesToScroll }
							onChange={ ( value ) => {
								setAttributes( {
									slidesToScroll: value,
								} );
							} }
							min={ 1 }
							max={ images.length }
						/>
					</PanelBody>
				</Fragment>
			</InspectorControls>

			<div { ...blockProps }>
				{ images.length > 0 ? (
					<Slider { ...settings }>
						{ images.map( ( image ) => (
							<div key={ image.id }>
								<img
									src={ image.sizes[ 'full' ].url }
									alt={ image.alt }
								/>
							</div>
						) ) }
					</Slider>
				) : (
					<p>{ __( 'Add some images!', 'beb-slider-gallery' ) }</p>
				) }
			</div>
		</Fragment>
	);
}
