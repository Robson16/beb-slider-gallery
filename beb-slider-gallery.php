<?php

/**
 * Plugin Name:       BeB Slider Gallery
 * Plugin URI:        https://gitlab.com/Robson16/beb-slider-gallery
 * Description:       A slider gallery block.
 * Requires at least: 6.1
 * Requires PHP:      7.0
 * Version:           1.0.1
 * Author:            Robson H. Rodrigues
 * Author URI:        https://www.linkedin.com/in/robson-h-rodrigues-93341746/
 * License:           MIT
 * Text Domain:       beb-slider-gallery
 * Domain Path:       /languages
 *
 * @package           create-block
 */

function beb_slider_gallery_block_init()
{
	register_block_type(__DIR__ . '/build');

	wp_set_script_translations(
		'beb-slider-gallery-editor-script',
		'beb-slider-gallery',
		plugin_dir_path(__FILE__) . 'languages'
	);

	wp_set_script_translations(
		'beb-slider-gallery-save-script',
		'beb-slider-gallery',
		plugin_dir_path(__FILE__) . 'languages'
	);
}
add_action('init', 'beb_slider_gallery_block_init');

/**
 * Enqueue Style
 *
 */
function beb_slider_gallery_enqueue_style()
{
	wp_enqueue_style('slick-carousel', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css', null, '1.8.1', 'all');
	wp_enqueue_style('slick-carousel-theme', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css', null, '1.8.1', 'all');
}
add_action('wp_enqueue_scripts', 'beb_slider_gallery_enqueue_style');
