��          �       |      |     }     �     �  	   �     �     �  
   �     �     �     �               0     >     P     `  	   f  /   p     �  .   �  8   �  �    -   �     
     $  	   7     A     J  
   Y     d     t     }     �     �     �     �     �  
   �     	  7        M  .   V  8   �   A custom slider gallery block. Add some images! BeB Slider Gallery Direction Dots Edit Images Horizontal Imagens Size Infinite Robson H. Rodrigues Select Images Select the images Slider Option Slides to Scroll  Slides to Show  Speed Swipeable Transition speed between slides in milliseconds Vertical https://gitlab.com/Robson16/beb-slider-gallery https://www.linkedin.com/in/robson-h-rodrigues-93341746/ Project-Id-Version: BeB Slider Gallery
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2023-05-15 15:10+0000
PO-Revision-Date: 2023-05-15 15:57+0000
Last-Translator: Agência B&amp;B
Language-Team: Português do Brasil
Language: pt_BR
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.6.4; wp-6.2
X-Domain: beb-slider-gallery Um bloco de galeria deslizante personalizado. Adicione algumas imagens! BeB Slider Gallery Direção Bolinhas Editar imagens Horizontal Imagens Tamanho Infinito Robson H. Rodrigues Selecionar imagens Selecione as imagens Opção de controle deslizante Slides para rolar Slides para Mostrar Velocidade Deslizável Velocidade de transição entre slides em milissegundos Vertical https://gitlab.com/Robson16/beb-slider-gallery https://www.linkedin.com/in/robson-h-rodrigues-93341746/ 